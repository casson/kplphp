<?php
/**
 * 表格构建器 主要文件
 */
namespace app\common\kbuilder\see;
use think\App;
use think\facade\Cookie;
use think\facade\Db;

class Kbuilder{
    protected $template;
    protected $where;
    protected $info;
    protected $vars = [];
    protected $list = [];

    public function __construct()
    {
        $this->initialize();
    }

    public function initialize()
    {
        $this->template = app()->getBasePath().'common/kbuilder/see/layout.html';
    }

    /**
     * @param null $id == 获取信息ID值
     * @return $this
     */
    public function getInfo($id=null)
    {
        $cacheFields = json_decode(Cookie::get('seeField'));
        $resultFields = [];
        foreach ($cacheFields as $k => $v)
        {
            $resultFields[$k]['field'] = $v[0];
            $resultFields[$k]['title'] = $v[1];
            $resultFields[$k]['mean'] = $v[2];
            $resultFields[$k]['css'] = $v[3];
            $resultFields[$k]['value'] = Db::table('kpl_csvceshi')->where(['id'=>$id])->value($v[0]);
        }
        $this->info = $resultFields;
        return $this;
    }

    /**
     * 渲染模板输出
     * @return \think\response\View
     */
    public function view()
    {
        $vars = [
            'data'=>$this->info
        ];
        return view($this->template,$vars);
    }
}